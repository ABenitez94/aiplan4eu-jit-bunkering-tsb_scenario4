import os
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd
from tkinter.messagebox import showinfo
import csv
import unified_planning
from unified_planning.shortcuts import *
from unified_planning.plans import SequentialPlan, ActionInstance
from datetime import datetime as dtime
import datetime
import time
from unified_planning.plot import (
    plot_plan,  # plot_plan plots all the types of plans, but is not customizable, while specific methods
    plot_sequential_plan,
    plot_time_triggered_plan,
    plot_partial_order_plan,
    plot_contingent_plan,
    plot_stn_plan,
    plot_causal_graph,
)

        
# Input data collection

class Data:
    def __init__(self ,iD, start_time, deltaAnch_Capacity):
        self.iD = iD
        self.start_time = datetime.datetime(int(start_time.split('/')[2].split(' ')[0]), int(start_time.split('/')[0]), int(start_time.split('/')[1])
                                            , int(start_time.split(' ')[1].split(':')[0]), int(start_time.split(' ')[1].split(':')[1]), 0).timestamp()
         
        self.deltaAnch_Capacity = int(deltaAnch_Capacity)      
        


class Action:
    def __init__(self ,name, start_time, end_time, duration, origin, destine):
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration
        self.origin = origin
        self.destine = destine

class Vessel:

    def __init__(self, id, name, bunkeringProvider, bunkeringTime,
                  timeToAnchorage, timeFromAnchorage,
                 timeToWaitingPoint, timeFromWaitingPoint, startAtAnchorage,
                 timeStartAtAnchorage, fuelRequested,fuelActuallyDelivered, etaAnch,
                 ataPBP, ataBerth, realWaitingTime, atsBunk, atcBunk, atdBerth, atdPBP,
                 *args,**kargs):
        
        self.id = id
        self.name = name
        self.bunkeringProvider = bunkeringProvider
        self.bunkeringTime = int(bunkeringTime) if bunkeringTime.isnumeric() else 0
        self.timeToAnchorage = int(timeToAnchorage) if timeToAnchorage.isnumeric() else 0
        self.timeFromAnchorage = int(timeFromAnchorage) if timeFromAnchorage.isnumeric() else 0
        self.timeToWaitingPoint = int(timeToWaitingPoint) if timeToWaitingPoint.isnumeric() else 0
        self.timeFromWaitingPoint = int(timeFromWaitingPoint) if timeFromWaitingPoint.isnumeric() else 0
        self.startAtAnchorage = startAtAnchorage
        self.timeStartAtAnchorage = int(timeStartAtAnchorage) if timeStartAtAnchorage.isnumeric() else 0
        self.fuelRequested = fuelRequested
        self.actions = []
        self.priority = 0
        self.waitingTime = 0
        self.fuelActuallyDelivered = fuelActuallyDelivered
        self.etaAnch = etaAnch
        self.ataPBP = ataPBP
        self.ataBerth = ataBerth
        self.realWaitingTime = realWaitingTime
        self.atsBunk = atsBunk
        self.atcBunk = atcBunk
        self.atdBerth = atdBerth
        self.atdPBP = atdPBP
        
class Barge:
    def __init__(self, id,name, companyLegalCode, refuelingPoint,
                 baseStations,timeToRefueling,
                 fuelAvailable, locationStartID):
        self.id = id
        self.name = name
        self.companyLegalCode = companyLegalCode
        self.refuelingPoint = refuelingPoint
        self.baseStations = baseStations
        self.timeToRefueling = timeToRefueling
        self.fuelCapacity = fuelAvailable
        self.fuelAvailable = fuelAvailable
        self.locationStartID = locationStartID
        self.actions = []
        self.idleTime = 0
        
#Class Inactividad Gabarras 
class BargeInactivityPeriod:
    def __init__(self ,inact_ID, bargeName, inact_timeStart, inact_Duration,
                 inact_BargeEndLocation):
        self.inact_ID = inact_ID
        self.bargeName = bargeName
        self.inact_timeStart = inact_timeStart
        self.inact_Duration = inact_Duration
        self.inact_BargeEndLocation = inact_BargeEndLocation
        
        
        
        #Clase Location Gabarras 
class BargeLocation:
    def __init__(self ,location_ID, location_name, location_type, location_capacity,
                 location_timeToAnch, used):
        self.location_ID = location_ID
        self.location_name = location_name
        self.location_type = location_type
        self.location_capacity = location_capacity
        self.location_timeToAnch = location_timeToAnch
        self.used = used


        #Clase Time_between_locations
class Time_between_locations:
    def __init__(self ,time_ID, time_name, time_betwen_locs):
        self.time_ID = time_ID
        self.time_name = time_name
        self.time_betwen_locs = time_betwen_locs

        
        
        
        #Clase Barges_actual_events 
class Barges_actual_events:
    def __init__(self ,iD, name, typeB, initTime, endTime, origin, destination, duration):
        self.iD = iD
        self.name = name
        self.type = typeB
        self.initTime = initTime
        self.endTime = endTime
        self.origin = origin
        self.destination = destination
        self.duration = duration
    
       

listVessels = []
listBarges = []
listBargesLocations = []
listBargesInactivityPeriod = []
listData = []
listTime_betwen_locs = []
listBargesActualEvents = []



# # create interface
# root = tk.Tk()
# root.title('Bunkering')
# root.resizable(False, False)
# root.geometry('400x400')

directory = fd.askdirectory()

# ask file Data
filenameData = ""
filetypes = (
        ('Csv', '*.csv'),
        ('All files', '*.*')
    )
filenameData = f'{directory}/General.csv'



# ask file Vessels
filenameVessel = ""
filetypes = (
        ('Csv', '*.csv'),
        ('All files', '*.*')
    )
filenameVessel =  f'{directory}/Vessels.csv'


# ask file barges
filenameBarge = ""
filetypes = (
        ('Csv', '*.csv'),
        ('All files', '*.*')
    )
filenameBarge =  f'{directory}/Barges.csv'


# ask file barges inactivity Period
filenameBargeInactivityPeriod = ""
filetypes = (
        ('Csv', '*.csv'),
        ('All files', '*.*')
    )
filenameBargeInactivityPeriod =  f'{directory}/Barges_inactivity_periods.csv'


# ask file barges location
filenameBargeLocation = ""
filetypes = (
        ('Csv', '*.csv'),
        ('All files', '*.*')
    )
filenameBargeLocation =  f'{directory}/Locations.csv'


# ask file Time_between_locations
filenameTime_between_locations = ""
filetypes = (
        ('Csv', '*.csv'),
        ('All files', '*.*')
    )
filenameTime_between_locations =  f'{directory}/Time_between_locations.csv'


# ask file Barges_actual_events
filenameBarges_actual_events = ""
filetypes = (
        ('Csv', '*.csv'),
        ('All files', '*.*')
    )
filenameBarges_actual_events =  f'{directory}/Barges_actual_events.csv'


# Read Csv


    #Leer Archivo de Data
with open(filenameData) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    column_count = 0
    auxCount = 1
    for row in csv_reader:
        if line_count == 0:
            for column in row:
                if 'Start_Time' == column:
                    posDStart_Time = column_count
                elif 'Anchorage_capacity' == column:
                    posDDeltaAnch_capacity = column_count

                column_count += 1
        elif line_count!=0:
            listData.append(Data(auxCount, row[posDStart_Time], row[posDDeltaAnch_capacity]))
            auxCount = auxCount + 1
        line_count += 1
    
    



    #Leer Archivo de Vessel
with open(filenameVessel) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    column_count = 0
    auxCount = 1
    for row in csv_reader:
        if line_count == 0:
            for column in row:
                if 'Vessel_name' == column:
                    posVName = column_count
                elif 'Vessel_bunkeringProvider' == column:
                    posVBunkProvider = column_count
                elif 'Bunkering_time' == column:
                    posVBunkeringTime = column_count
                elif 'Vessel_timeToAnchorage' == column:
                    posVTimeToAnch = column_count
                elif 'Vessel_timeFromAnchorage' == column:
                    posVTimeFromAnch = column_count
                elif 'Vessel_timeToWaitingPoint' == column:
                    posVTimeToWP = column_count
                elif 'Vessel_timeFromWaitingPoint' == column:
                    posVTimeFromWP = column_count
                elif 'Vessel_startAtAnchorage' == column:
                    posVStartAtAnch = column_count
                elif 'Vessel_timeStartAtAnchorage' == column:
                    posVTimeStartAtAnch = column_count
                elif 'Vessel_fuelRequested' == column:
                    posVFuelRequested = column_count
                    
                elif 'Vessel_fuelActuallyDelivered' == column:
                    posVFuelAct = column_count
                elif 'Vessel_ETA_Anchorage' == column:
                    posVEtaAnch = column_count
                elif 'Vessel_ATA_PBP' == column:
                    posVATAPBP = column_count
                elif 'Vessel_ATA_Berth' == column:
                    posVATABerth = column_count
                elif 'Vessel_waitingTime' == column:
                    posVWaitingTime = column_count
                elif 'Vessel_ATS_Bunkering' == column:
                    posVATSBunk = column_count
                elif 'Vessel_ATC_Bunkering' == column:
                    posVATCBunk = column_count
                elif 'Vessel_ATD_Berth' == column:
                    posVATDBerth = column_count
                elif 'Vessel_ATD_PBP' == column:
                    posVATDPBP = column_count
                    
                column_count += 1
        elif line_count!=0:
            listVessels.append(Vessel(auxCount, row[posVName], row[posVBunkProvider], row[posVBunkeringTime],
                                          row[posVTimeToAnch], row[posVTimeFromAnch], row[posVTimeToWP], row[posVTimeFromWP],
                                        row[posVStartAtAnch], row[posVTimeStartAtAnch], row[posVFuelRequested],
                                        row[posVFuelAct], row[posVEtaAnch], row[posVATAPBP],
                                        row[posVATABerth], row[posVWaitingTime], row[posVATSBunk],
                                        row[posVATCBunk],row[posVATDBerth], row[posVATDPBP]))
            auxCount = auxCount + 1
        line_count += 1
        
        
        
    #Leer Archivo de BargeInactivityPeriod
with open(filenameBargeInactivityPeriod) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    column_count = 0
    auxCount = 1
    for row in csv_reader:
        if line_count == 0:
            for column in row:
                if 'BargeInact_name' == column:
                    posIBargeID = column_count
                elif 'BargeInact_timeStart' == column:
                    posITimeStart = column_count
                elif 'BargeInact_duration' == column:
                    posIDuration = column_count
                elif 'BargeInact_endLocation' == column:
                    posIBargeEndLocation = column_count                 

                column_count += 1
                
        elif line_count!=0:
            listBargesInactivityPeriod.append(BargeInactivityPeriod(auxCount, row[posIBargeID], row[posITimeStart], 
                                         row[posIDuration], row[posIBargeEndLocation]))
            auxCount = auxCount + 1
        line_count += 1
        
listLocID = {}   
# Barge Location
with open(filenameBargeLocation) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    column_count = 0
    for row in csv_reader:
        if line_count == 0:
            for column in row:
                if 'Location_id' == column:
                    posLID = column_count
                elif 'Location_name' == column:
                    posLName = column_count
                elif 'Location_type' == column:
                    posLType = column_count
                elif 'Location_capacity' == column:
                    posLCapacity = column_count
                elif 'Location_timeToAnchorage' == column:
                    posLTimeToAnch = column_count                 

                column_count += 1
                
        elif line_count!=0:
            listBargesLocations.append(BargeLocation(row[posLID], row[posLName],
                                        row[posLType], row[posLCapacity], row[posLTimeToAnch], False))
            listLocID.update({row[posLID]:row[posLName]})
        line_count += 1
            

        
    #Leer Archivo de Barge
with open(filenameBarge) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    column_count = 0
    auxCount = 1
    for row in csv_reader:
        if line_count == 0:
            for column in row:
                if 'Barge_name' == column:
                    posBName = column_count
                elif 'Barge_companyLegalCode' == column:
                    posBCompanyLegalCode = column_count
                elif 'Barge_refuelingPoint' == column:
                    posBRefuelPoint = column_count
                elif 'Barge_baseStations' == column:
                    posBBaseStation = column_count
                elif 'Barge_timeToRefueling' == column:
                    posBTimeToRefuel = column_count
                elif 'Barge_fuelAvailable' == column:
                    posBFuelAvailable = column_count
                elif 'Barge_locationStartId' == column:
                    posBLocationStartID = column_count

                column_count += 1
        elif line_count!=0:
            listBarges.append(Barge(auxCount, row[posBName], row[posBCompanyLegalCode], row[posBRefuelPoint], row[posBBaseStation],
                                          row[posBTimeToRefuel], row[posBFuelAvailable], row[posBLocationStartID]))
            auxCount = auxCount + 1

        line_count += 1



# Time_between_locations
with open(filenameTime_between_locations) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    column_count = 0
    auxColumn = []
    auxRow = None
    for row in csv_reader:
        if line_count == 0:
            for column in row:
                if 'Locations/Locations' == column:
                    posTime_ID = column_count
                elif 'Location_name' == column:
                    posTime_name = column_count
                else:
                    auxColumn.append(column)
                auxRow = row
                column_count += 1
                
        elif line_count!=0:
            auxInt = 0
            auxTBL = {}
            countF = 0
            for j in auxRow:
                for k in listBargesLocations:
                    if j in k.location_ID:
                        auxRow[countF] = k.location_name
                countF = countF + 1
            
            for i in auxColumn:
                auxTBL.update({ auxRow[1 + auxInt] : row[1 + auxInt]})
                auxInt = auxInt + 1
                    
            listTime_betwen_locs.append(Time_between_locations(row[posTime_ID], row[posTime_name],
                                         auxTBL))
        line_count += 1
            


# Barge Location
with open(filenameBarges_actual_events) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    column_count = 0
    countID = 0
    for row in csv_reader:
        if line_count == 0:
            for column in row:
                if 'BargeActualEvent_name' == column:
                    posBarges_actual_events_name = column_count
                elif 'BargeActualEvent_type' == column:
                    posBarges_actual_events_type  = column_count
                elif 'BargeActualEvent_InitTime' == column:
                    posBarges_actual_events_initTime = column_count
                elif 'BargeActualEvent_endTime' == column:
                    posBarges_actual_events_endTime = column_count    
                elif 'BargeActualEvent_origin' == column:
                    posBarges_actual_events_origin = column_count    
                elif 'BargeActualEvent_destination' == column:
                    posBarges_actual_events_destination = column_count    
                elif 'BargeActualEvent_duration' == column:
                    posBarges_actual_events_duration = column_count                 

                column_count += 1
                
        elif line_count!=0:
            listBargesActualEvents.append(Barges_actual_events(countID, row[posBarges_actual_events_name], row[posBarges_actual_events_type], 
                                         row[posBarges_actual_events_initTime], row[posBarges_actual_events_endTime], row[posBarges_actual_events_origin],
                                          row[posBarges_actual_events_destination], row[posBarges_actual_events_duration]))
            countID = countID + 1
        line_count += 1
            



# PRIORIDAD
def timeToAnch(e):
  return e.timeToAnchorage
# PRIORIDAD
def timeStartAtAnch(e):
  return e.timeStartAtAnchorage


count = 1
listVessels.sort(key=timeStartAtAnch)
for vessel in listVessels:

    if vessel.startAtAnchorage == "True":
        
        vessel.priority = count
        print (f"{vessel.name} - {vessel.timeStartAtAnchorage} - {vessel.priority}")
        count=count+1

listVessels.sort(key=timeToAnch)
for vessel in listVessels:

    if vessel.startAtAnchorage == "False":
        
        vessel.priority = count
        print (f"{vessel.name} - {vessel.timeToAnchorage} - {vessel.priority}")
        count=count+1





# -------------------------------------------------------------------

# Define PDDL problem elements 

# Variables PPDL

Vessel = UserType('Vessel')
Barge = UserType('Barge')
Position = UserType('Position')




# Define Fluents
vessel_on = unified_planning.model.Fluent('vessel-on', BoolType(), v=Vessel, p=Position)
barge_on = unified_planning.model.Fluent('barge-on', BoolType(), b=Barge, p=Position)
isFueled = unified_planning.model.Fluent('isFueled', BoolType(), v=Vessel)
isAnch = unified_planning.model.Fluent('isAnch', BoolType(), p=Position)
isRefuelZone = unified_planning.model.Fluent('isRefuelZone', BoolType(), p=Position)
vesselInAnch = unified_planning.model.Fluent('VesselInAnch', BoolType(), v=Vessel)

bargeInUse = unified_planning.model.Fluent('bargeInUse', BoolType(), b=Barge)
positionFree = unified_planning.model.Fluent('positionFree', BoolType(), p=Position)
vesselBargePosition = unified_planning.model.Fluent('vesselBargePosition', BoolType(), v=Vessel, b=Barge, p=Position)
timeBargeRefueling = unified_planning.model.Fluent('timeBargeRefueling', IntType(), b = Barge)

timeInitToAnch = unified_planning.model.Fluent('timeInitToAnch', IntType(), v=Vessel)
timeInitToWait = unified_planning.model.Fluent('timeInitToWait', IntType(), v=Vessel)
timeWaitToAnch = unified_planning.model.Fluent('timeWaitToAnch', IntType(), v=Vessel)
timeAnchToExit = unified_planning.model.Fluent('timeAnchToExit', IntType(), v=Vessel)
timeBunkering = unified_planning.model.Fluent('timeBunkering', IntType(),  v=Vessel)
timeBargeLocationToAnch  = unified_planning.model.Fluent('timeBargeLocationToAnch', IntType(), p=Position)
timeVesselStartAtAnch = unified_planning.model.Fluent('timeVesselStartAtAnch', IntType(), v=Vessel)
timeBargeToRefuel = unified_planning.model.Fluent('timeBargeToRefuel', IntType(), p = Position, p0 = Position)


numberOfVessels  = unified_planning.model.Fluent('numberOfVessels', IntType())
bunkeringProviderVessel = unified_planning.model.Fluent('BunkeringProviderVessel', IntType(), v=Vessel)
bunkeringProviderBarge = unified_planning.model.Fluent('BunkeringProviderBarge ', IntType(),  b=Barge)
fuelRequiredVessel = unified_planning.model.Fluent('fuelRequiredVessel', IntType(), v=Vessel)
totalFuelBarge = unified_planning.model.Fluent('totalFuelBarge', IntType(), b=Barge)
actualFuelBarge = unified_planning.model.Fluent('actualFuelBarge', IntType(), b=Barge)

priorityGlobal = unified_planning.model.Fluent('priority', IntType())

priorityVessel = unified_planning.model.Fluent('priorityVessel', IntType(), v=Vessel)



    # Define Objects
    
startPoint = unified_planning.model.Object('StartPoint', Position)
waitingPoint = unified_planning.model.Object('WaitingPoint', Position)
exit = unified_planning.model.Object('Exit', Position)




    # StartPoint_to_Anchorage
startPoint_to_Anchorage = DurativeAction('StartPoint_to_Anch', v=Vessel, b=Barge, p0=Position, p=Position)
v = startPoint_to_Anchorage.parameter('v')
b = startPoint_to_Anchorage.parameter('b')
p = startPoint_to_Anchorage.parameter('p')
p0 = startPoint_to_Anchorage.parameter('p0')

    # Duration
startPoint_to_Anchorage.set_fixed_duration(timeInitToAnch(v))

    # Start Precondition
    
startPoint_to_Anchorage.add_condition(StartTiming(), Equals(priorityVessel(v), priorityGlobal))
startPoint_to_Anchorage.add_condition(StartTiming(), vessel_on(v, startPoint))
startPoint_to_Anchorage.add_condition(StartTiming(), isAnch(p))
startPoint_to_Anchorage.add_condition(StartTiming(), Equals(bunkeringProviderVessel(v), bunkeringProviderBarge(b)))
startPoint_to_Anchorage.add_condition(StartTiming(), barge_on(b, p0))

startPoint_to_Anchorage.add_condition(StartTiming(), GT(timeInitToAnch(v), timeBargeLocationToAnch(p0)))



    # End Precondition
    
startPoint_to_Anchorage.add_condition(EndTiming(), vesselBargePosition(v, b, p))
startPoint_to_Anchorage.add_condition(EndTiming(), positionFree(p))

startPoint_to_Anchorage.add_condition(EndTiming(), bargeInUse(b))
startPoint_to_Anchorage.add_condition(EndTiming(), barge_on(b, p))
startPoint_to_Anchorage.add_condition(EndTiming(), LE(fuelRequiredVessel(v), actualFuelBarge(b)))



    # Start Action
    
startPoint_to_Anchorage.add_effect(StartTiming(), vessel_on(v, startPoint), False)
startPoint_to_Anchorage.add_effect(StartTiming(), vesselBargePosition(v, b, p), True)
startPoint_to_Anchorage.add_effect(StartTiming(), numberOfVessels, numberOfVessels-1)
startPoint_to_Anchorage.add_effect(StartTiming(), priorityGlobal, priorityGlobal+1)


    # End Action
startPoint_to_Anchorage.add_effect(EndTiming(), vessel_on(v, p), True)
startPoint_to_Anchorage.add_effect(EndTiming(), vesselInAnch(v), True)
startPoint_to_Anchorage.add_effect(EndTiming(), positionFree(p), False)





    # StartPoint_to_WaitingPoint 
startPoint_to_WaitingPoint = DurativeAction('StartPoint_to_WaitingPoint', v=Vessel)
v = startPoint_to_WaitingPoint.parameter('v')

    # Duration
startPoint_to_WaitingPoint.set_fixed_duration(timeInitToWait(v))

    # Start Precondition
    
startPoint_to_WaitingPoint.add_condition(StartTiming(), Equals(priorityVessel(v), priorityGlobal))
startPoint_to_WaitingPoint.add_condition(StartTiming(), vessel_on(v, startPoint))

    # Start Action
    
startPoint_to_WaitingPoint.add_effect(StartTiming(), vessel_on(v, startPoint), False)
startPoint_to_WaitingPoint.add_effect(StartTiming(), numberOfVessels, numberOfVessels-1)
startPoint_to_WaitingPoint.add_effect(StartTiming(), priorityGlobal, priorityGlobal+1)


    # End Action
startPoint_to_WaitingPoint.add_effect(EndTiming(), vessel_on(v, waitingPoint), True)




    # WaitingPoint_to_Anchorage
waitingPoint_to_Anchorage = DurativeAction('WaitingPoint_to_Anch', v=Vessel, b=Barge, p=Position)
v = waitingPoint_to_Anchorage.parameter('v')
b = waitingPoint_to_Anchorage.parameter('b')
p = waitingPoint_to_Anchorage.parameter('p')

    # Duration
waitingPoint_to_Anchorage.set_fixed_duration(timeWaitToAnch(v))

    # Start Precondition
    
waitingPoint_to_Anchorage.add_condition(StartTiming(), Equals(numberOfVessels, 0))
waitingPoint_to_Anchorage.add_condition(StartTiming(), vessel_on(v, waitingPoint))
waitingPoint_to_Anchorage.add_condition(StartTiming(), Equals(bunkeringProviderVessel(v), bunkeringProviderBarge(b)))

waitingPoint_to_Anchorage.add_condition(StartTiming(), isAnch(p))



    # End Precondition
    
waitingPoint_to_Anchorage.add_condition(EndTiming(), vesselBargePosition(v, b, p))
waitingPoint_to_Anchorage.add_condition(EndTiming(), positionFree(p))

waitingPoint_to_Anchorage.add_condition(EndTiming(), bargeInUse(b))
waitingPoint_to_Anchorage.add_condition(EndTiming(), barge_on(b, p))

waitingPoint_to_Anchorage.add_condition(EndTiming(), LE(fuelRequiredVessel(v), actualFuelBarge(b)))



    # Start Action
waitingPoint_to_Anchorage.add_effect(StartTiming(), vessel_on(v, waitingPoint), False)
waitingPoint_to_Anchorage.add_effect(StartTiming(), vesselBargePosition(v, b, p), True)

    # End Action
waitingPoint_to_Anchorage.add_effect(EndTiming(), vessel_on(v, p), True)
waitingPoint_to_Anchorage.add_effect(EndTiming(), positionFree(p), False)
waitingPoint_to_Anchorage.add_effect(EndTiming(), vesselInAnch(v), True)



    # BargeLocation_to_Anchorage_VesselFurther 
bargeLocation_to_Anchorage = DurativeAction('BargeLocation_to_Anch', b=Barge, v=Vessel, p0=Position, p=Position)
b = bargeLocation_to_Anchorage.parameter('b')
v = bargeLocation_to_Anchorage.parameter('v')
p0 = bargeLocation_to_Anchorage.parameter('p0')
p = bargeLocation_to_Anchorage.parameter('p')

    # Duration
bargeLocation_to_Anchorage.set_fixed_duration(timeBargeLocationToAnch(p0))

    # Start Precondition
    
bargeLocation_to_Anchorage.add_condition(StartTiming(), Equals(numberOfVessels, 0))


bargeLocation_to_Anchorage.add_condition(StartTiming(), Equals(bunkeringProviderVessel(v), bunkeringProviderBarge(b)))
bargeLocation_to_Anchorage.add_condition(StartTiming(), Not(vesselInAnch(v)))
bargeLocation_to_Anchorage.add_condition(StartTiming(), Not(vessel_on(v, startPoint)))

bargeLocation_to_Anchorage.add_condition(StartTiming(), Not(bargeInUse(b)))
bargeLocation_to_Anchorage.add_condition(StartTiming(), Not(isAnch(p0)))

bargeLocation_to_Anchorage.add_condition(StartTiming(), vesselBargePosition(v, b, p))

bargeLocation_to_Anchorage.add_condition(StartTiming(), barge_on(b, p0))
bargeLocation_to_Anchorage.add_condition(StartTiming(), isAnch(p))


    # End Precondition
bargeLocation_to_Anchorage.add_condition(EndTiming(), positionFree(p))


    # Start Action
bargeLocation_to_Anchorage.add_effect(StartTiming(), barge_on(b, p0), False)
bargeLocation_to_Anchorage.add_effect(StartTiming(), bargeInUse(b), True)

    # End Action
bargeLocation_to_Anchorage.add_effect(EndTiming(), barge_on(b, p), True)


    # Bunkering  
bunkering = DurativeAction('Bunkering', v=Vessel, b=Barge, p=Position)
v = bunkering.parameter('v')
b = bunkering.parameter('b')
p = bunkering.parameter('p')

    # Duration
bunkering.set_fixed_duration(timeBunkering(v))

    # Start Precondition
    
bunkering.add_condition(StartTiming(), Equals(numberOfVessels, 0))

# bunkering.add_condition(StartTiming(), active(b))
bunkering.add_condition(StartTiming(), vesselBargePosition(v, b, p))
bunkering.add_condition(StartTiming(), vessel_on(v, p))
bunkering.add_condition(StartTiming(), barge_on(b, p))
bunkering.add_condition(StartTiming(), bargeInUse(b))


    # Start Action



    # End Action
bunkering.add_effect(EndTiming(), vesselBargePosition(v, b, p), False)
bunkering.add_effect(EndTiming(), bargeInUse(b), False)
bunkering.add_effect(EndTiming(), isFueled(v), True)
bunkering.add_effect(EndTiming(), actualFuelBarge(b), actualFuelBarge(b)-fuelRequiredVessel(v))



    # Anchorage_to_Exit   
anchorage_to_Exit = DurativeAction('Anch_to_Exit', v=Vessel, p=Position)
v = anchorage_to_Exit.parameter('v')
p = anchorage_to_Exit.parameter('p')

    # Duration
anchorage_to_Exit.set_fixed_duration(timeAnchToExit(v))

    # Start Precondition
    
anchorage_to_Exit.add_condition(StartTiming(), Equals(numberOfVessels, 0))
anchorage_to_Exit.add_condition(StartTiming(), vessel_on(v, p))
anchorage_to_Exit.add_condition(StartTiming(), isAnch(p))
anchorage_to_Exit.add_condition(StartTiming(), isFueled(v))

    # Start Action
anchorage_to_Exit.add_effect(StartTiming(), vessel_on(v, p), False)
anchorage_to_Exit.add_effect(StartTiming(), positionFree(p), True)


    # End Action
anchorage_to_Exit.add_effect(EndTiming(), vessel_on(v, exit), True)




    # BargeOnSite   
bargeOnSite = DurativeAction('BargeOnSite', b=Barge, v=Vessel, p=Position)
v = bargeOnSite.parameter('v')
p = bargeOnSite.parameter('p')
b = bargeOnSite.parameter('b')
    # Duration
bargeOnSite.set_fixed_duration(0)

    # Start Precondition
    
bargeOnSite.add_condition(StartTiming(), Equals(numberOfVessels, 0))
bargeOnSite.add_condition(StartTiming(), vesselBargePosition(v, b, p))
# bargeOnSite.add_condition(StartTiming(), active(b))
bargeOnSite.add_condition(StartTiming(), Not(bargeInUse(b)))
bargeOnSite.add_condition(StartTiming(), barge_on(b, p))

    # Start Action
bargeOnSite.add_effect(StartTiming(), bargeInUse(b), True)

    # VesselAtAnchorageAtStart 
vesselAtAnchorageAtStart = DurativeAction('VesselAtAnchAtStart', v=Vessel, p=Position)
v = vesselAtAnchorageAtStart.parameter('v')
p = vesselAtAnchorageAtStart.parameter('p')

    # Duration
vesselAtAnchorageAtStart.set_fixed_duration(timeVesselStartAtAnch(v))

    # Start Precondition
vesselAtAnchorageAtStart.add_condition(StartTiming(), Equals(priorityVessel(v), priorityGlobal))
vesselAtAnchorageAtStart.add_condition(StartTiming(), GT(timeVesselStartAtAnch(v), 0))
vesselAtAnchorageAtStart.add_condition(StartTiming(), isAnch(p))
vesselAtAnchorageAtStart.add_condition(StartTiming(), Not(positionFree(p)))
vesselAtAnchorageAtStart.add_condition(StartTiming(), vessel_on(v, p))


    # Start Action
vesselAtAnchorageAtStart.add_effect(StartTiming(), numberOfVessels, numberOfVessels-1)
vesselAtAnchorageAtStart.add_effect(StartTiming(), priorityGlobal, priorityGlobal+1)

    # End Action
vesselAtAnchorageAtStart.add_effect(EndTiming(), isFueled(v), True)





    # refueling
refueling = DurativeAction('Refueling Barge', b=Barge, p=Position)
b = refueling.parameter('b')
p = refueling.parameter('p')

refueling.set_fixed_duration(timeBargeRefueling(b))
refueling.add_condition(StartTiming(),  Equals(numberOfVessels, 0))
refueling.add_condition(StartTiming(), isRefuelZone(p))
refueling.add_condition(StartTiming(), barge_on(b, p))
refueling.add_condition(StartTiming(), LT(actualFuelBarge(b), totalFuelBarge(b)))



refueling.add_effect(EndTiming(), actualFuelBarge(b), totalFuelBarge(b))


    # barge_to_refueling
barge_to_refueling = DurativeAction('barge_to_refueling', b=Barge, p0=Position, p = Position)
b = barge_to_refueling.parameter('b')
p = barge_to_refueling.parameter('p')
p0 = barge_to_refueling.parameter('p0')

barge_to_refueling.set_fixed_duration(timeBargeToRefuel(p, p0))

barge_to_refueling.add_condition(StartTiming(), Equals(numberOfVessels, 0))
barge_to_refueling.add_condition(StartTiming(), Not(bargeInUse(b)))

barge_to_refueling.add_condition(StartTiming(), barge_on(b, p0))
barge_to_refueling.add_condition(StartTiming(), isRefuelZone(p))


barge_to_refueling.add_effect(StartTiming(), barge_on(b, p0), False)

barge_to_refueling.add_effect(EndTiming(), barge_on(b, p), True)


# Define Problem
problem = Problem('problemBunkering')

    # Add Fluents
problem.add_fluent(vessel_on, default_initial_value=False)
problem.add_fluent(barge_on, default_initial_value=False)
problem.add_fluent(isFueled, default_initial_value=False)
problem.add_fluent(isAnch, default_initial_value=False)
problem.add_fluent(bargeInUse, default_initial_value=False)
problem.add_fluent(positionFree, default_initial_value=True)
problem.add_fluent(vesselBargePosition, default_initial_value=False)
problem.add_fluent(timeInitToAnch, default_initial_value=0)
problem.add_fluent(timeInitToWait, default_initial_value=0)
problem.add_fluent(timeWaitToAnch, default_initial_value=0)
problem.add_fluent(timeAnchToExit, default_initial_value=0)
problem.add_fluent(timeBunkering, default_initial_value=0)
problem.add_fluent(timeBargeLocationToAnch, default_initial_value=0)
problem.add_fluent(numberOfVessels, default_initial_value=len(listVessels))


problem.add_fluent(bunkeringProviderVessel, default_initial_value=0)
problem.add_fluent(bunkeringProviderBarge, default_initial_value=0)

problem.add_fluent(timeVesselStartAtAnch, default_initial_value=0)


problem.add_fluent(fuelRequiredVessel, default_initial_value=0)
problem.add_fluent(totalFuelBarge, default_initial_value=0)
problem.add_fluent(actualFuelBarge, default_initial_value=0)





problem.add_fluent(isRefuelZone, default_initial_value=False)
problem.add_fluent(vesselInAnch, default_initial_value=False)


problem.add_fluent(timeBargeRefueling, default_initial_value=0)
problem.add_fluent(timeBargeToRefuel, default_initial_value=0)

problem.add_fluent(priorityGlobal, default_initial_value=1)
problem.add_fluent(priorityVessel, default_initial_value=0)




#Add Objects


anchObjects = {}
for i in range(listData[0].deltaAnch_Capacity):
    aux = Object(f'Anchorage{i+1}', Position)
    problem.add_object(aux)

    problem.set_initial_value(timeBargeLocationToAnch(aux), 20)
    problem.set_initial_value(isAnch(aux), True)
    
    anchObjects.update({aux : False})
    



locationsBargeObject = {}
for location in listBargesLocations:    
    if location.location_type == 'Base Stations':
        aux = Object(f'{location.location_name}', Position)
        problem.add_object(aux)        
        problem.set_initial_value(timeBargeLocationToAnch(aux), location.location_timeToAnch)
        locationsBargeObject.update({aux : location.location_ID})
        
    elif location.location_type == 'Refueling Point':
        aux = unified_planning.model.Object(f'{location.location_name}', Position)
        problem.add_object(aux)        
        problem.set_initial_value(isRefuelZone(aux), True)
        
        problem.set_initial_value(timeBargeLocationToAnch(aux), location.location_timeToAnch)        
        locationsBargeObject.update({aux : location.location_ID})

    
for vessel in listVessels:
    aux = Object(vessel.name, Vessel)
    problem.add_object(aux)

    #Position
    if(vessel.startAtAnchorage == "False"):
        problem.set_initial_value(vessel_on(aux, startPoint), True)
    elif(vessel.startAtAnchorage == "True"):
        for anchObjectsKey, anchObjectsValue in anchObjects.items():
            if anchObjectsValue == False:
                problem.set_initial_value(vessel_on(aux, anchObjectsKey), True)                 
                problem.set_initial_value(vesselInAnch(aux), True) 
                anchObjects.update({anchObjectsKey : True})
                problem.set_initial_value(positionFree(anchObjectsKey), False)
                break 
            
            
    # Bunkering provider
    if vessel.bunkeringProvider[1:].isnumeric():
        problem.set_initial_value(bunkeringProviderVessel(aux), vessel.bunkeringProvider[1:])
    problem.set_initial_value(priorityVessel(aux), vessel.priority)    


    if vessel.fuelRequested.isnumeric():
        problem.set_initial_value(fuelRequiredVessel(aux), vessel.fuelRequested)
        
    # Times
        
    problem.set_initial_value(timeInitToAnch(aux), vessel.timeToAnchorage)
    problem.set_initial_value(timeInitToWait(aux), vessel.timeToWaitingPoint)
    problem.set_initial_value(timeWaitToAnch(aux), vessel.timeFromWaitingPoint)
    problem.set_initial_value(timeAnchToExit(aux), vessel.timeFromAnchorage)
    problem.set_initial_value(timeBunkering(aux), vessel.bunkeringTime)
    problem.set_initial_value(timeVesselStartAtAnch(aux), vessel.timeStartAtAnchorage)
        
        # Add Goal
    problem.add_timed_goal(EndTiming(), vessel_on(aux, exit))
        
        
    
    
for barge in listBarges:
    aux = Object(barge.name, Barge)
    problem.add_object(aux)
    problem.set_initial_value(bunkeringProviderBarge(aux), barge.companyLegalCode[1:]) 
    if barge.fuelAvailable.isnumeric():
        problem.set_initial_value(actualFuelBarge(aux), barge.fuelAvailable)        
        problem.set_initial_value(totalFuelBarge(aux), barge.fuelCapacity)
        problem.set_initial_value(timeBargeRefueling(aux), barge.timeToRefueling if barge.timeToRefueling != "--" else 0)
    

        #Positions

    for locObjectKey, locObjectValue in locationsBargeObject.items():
        if int(locObjectValue) == int(barge.locationStartID):
            if 'BaseStation' in locObjectKey.name:
                problem.set_initial_value(barge_on(aux, locObjectKey), True) 
                break 
        

# Relations between barge Location
for locBargeKey, locBargeValue in locationsBargeObject.items():
    if "RefuelingPoint" in locBargeKey.name:    
        for locBargeKey2, locBargeValue2 in locationsBargeObject.items():
            if locBargeKey.name != locBargeKey2.name:
                if locBargeKey.name[-10] != locBargeKey2.name[-10]:
                    for tb in listTime_betwen_locs:
                        if tb.time_name.lower() in locBargeKey.name.lower():
                            for tb2K, tb2V in tb.time_betwen_locs.items():       
                                if not tb2K.lower().isnumeric() and tb2K.lower() in locBargeKey2.name.lower():
                                    problem.set_initial_value(timeBargeToRefuel(locBargeKey2, locBargeKey), tb2V)
                                    
        
for locBargeKey in listBargesLocations:
    if "RefuelingPoint" in locBargeKey.location_name:
        for d in range(listData[0].deltaAnch_Capacity):
            problem.set_initial_value(timeBargeToRefuel(Object(f"Anchorage{d+1}", Position), Object(locBargeKey.location_name, Position)), locBargeKey.location_timeToAnch)
    

problem.add_object(startPoint)
problem.add_object(waitingPoint)

problem.add_object(exit)

    # Add Actions
problem.add_action(startPoint_to_Anchorage)

problem.add_action(startPoint_to_WaitingPoint)

problem.add_action(waitingPoint_to_Anchorage)


problem.add_action(bargeLocation_to_Anchorage)

problem.add_action(bunkering)

problem.add_action(anchorage_to_Exit)

problem.add_action(bargeOnSite)


problem.add_action(refueling)

problem.add_action(barge_to_refueling)





problem.add_action(vesselAtAnchorageAtStart)



# Add Timed Goal

problem.add_timed_goal(StartTiming(1), Equals(numberOfVessels, 0))







aa = ''

for bb in directory.split("/"):
    if bb not in directory.split("/")[-1]:
        aa = aa + '/' + bb


now = dtime.now()
folder = f'{aa[1:]}/Output/'
os.makedirs(folder, exist_ok=True)
with open(f'{aa[1:]}/pddlProblem.doc', mode='w', newline='') as file:
    file.write(f'{problem}')
        
        
        

# Solve Problem with OneshotPlanner
# Enhsp
with OneshotPlanner(name='tamer') as planner:
    result = planner.solve(problem)
    
    if result.status == up.engines.PlanGenerationResultStatus.SOLVED_SATISFICING:
         print("Tamer returned: %s" % result.plan)
         print(dtime.now())
    else:
         print("No plan found.")
         print(dtime.now())
         
         
strplan = ("%s" % result.plan)

planning = strplan.split('\n')


# Collect data through the resulting planning for the creation of Output


count = 1
planning_results= {}
barges_results = {}
barges_other = {}
vessels_results = {}

listAux = {}

listAuxBarge = {}


capacity = 0

for inst in planning:
    
    if "StartPoint_to_Anch" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        vessel = inst.split("(")[1].split(",")[0]
        destine = inst.split(",")[3].split(")")[0].strip()
            
        
        #Planning Result output
        planning_results.update({count : {'Item':count,'Action_InitTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime':dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'StartPoint_to_Anchorage'
                                        , 'Duration':duration, 'Vessel': vessel, 'Origin': "InitialZone"
                                        , 'Destine':destine, 'Anchorage Occupation':capacity}})
    
        #Vessel Output
        vessels_results.update({ f'{vessel}-RTA': endTime, f'{vessel}-timeToAnch': duration})
        
        
        count =count+1
        
    elif "StartPoint_to_WaitingPoint" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        vessel = inst.split("(")[1].split(",")[0].split(")")[0]
        
        #Planning Result output
        planning_results.update({count : {'Item':count,'Action_InitTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime':dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'StartPoint_to_WaitingPoint'
                                        , 'Duration':duration, 'Vessel': vessel, 'Origin': "InitialZone"
                                        , 'Destine':'Waiting Point', 'Anchorage Occupation':capacity}})
        
        #Vessel Output
        vessels_results.update({ f'{vessel}-RTA':endTime, f'{vessel}-timeToAnch': duration})
                
        listAux.update({vessel:endTime})
        count =count+1
        
        
    elif "WaitingPoint_to_Anch" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        vessel = inst.split("(")[1].split(",")[0]
        
        #Planning Result output
        
        for auxK, auxV in listAux.items():
            if auxK in vessel:
                planning_results.update({f'{count} - {vessel}' : {'Item':count,'Action_InitTime': dtime.fromtimestamp((int(auxV)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'Waiting Time Vessel'
                                        , 'Duration': int(initTime) - auxV, 'Vessel': vessel, 'Origin': "WaitingPoint"}})
                for ves in listVessels:
                    if ves.name in vessel:
                        ves.waitingTime = int(initTime) - auxV
        count =count+1
        planning_results.update({count : {'Item':count,'Action_InitTime': dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime': dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'WaitingPoint_to_Anchorage'
                                        , 'Duration':duration, 'Vessel': vessel, 'Origin': "WaitingPoint"
                                        , 'Destine':destine, 'Anchorage Occupation':capacity}})
        
        #Vessel Output
        vessels_results.update({ f'{vessel}-RTA':endTime, f'{vessel}-timeToAnch': duration})
                
        
        count =count+1
        
        
    elif "BargeLocation_to_Anch" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        barge = inst.split("(")[1].split(",")[0]
        vessel = inst.split(",")[1].split(",")[0].strip()
        origin = inst.split(",")[2].split(")")[0].strip()        
        destine = inst.split(",")[3].split(")")[0].strip()
        
        #Planning Result output
        planning_results.update({count : {'Item':count,'Action_InitTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime':dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'BargeLocation_to_Anchorage'
                                        , 'Duration':duration, 'Barge': barge, 'Vessel':vessel, 'Origin': origin
                                        , 'Destine':destine, 'Anchorage Occupation':capacity}})
        #Barge Output
        barges_results.update({f'{count} - {barge}'  : {'BargePlannedMovement_name': f"BargeLocation_to_Anchorage({vessel})", 'BargePlannedMovement_initTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 
                                        'BargePlannedMovement_EndTime': dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"),'BargePlannedMovement_Origin': origin,
                                        'BargePlannedMovement_Destine': destine,'BargePlannedMovement_Duration':duration}})
        listAuxBarge.update({barge:endTime})
        
        count =count+1   
        
        
    elif "Refueling_Barge" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        barge = inst.split("(")[1].split(",")[0]
        origin = inst.split(",")[1].split(")")[0].strip()        
        
        #Planning Result output
        planning_results.update({count : {'Item':count,'Action_InitTime': dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime': dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'Refueling Barge'
                                        , 'Duration':duration, 'Barge': barge,'Origin': origin, 'Anchorage Occupation':capacity}})
        #Barge Output
        barges_results.update({f'{count} - {barge}'  : {'BargePlannedMovement_name': f"Refueling Barge", 'BargePlannedMovement_initTime': dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 
                                        'BargePlannedMovement_EndTime': dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"),'BargePlannedMovement_Origin': origin, 'BargePlannedMovement_Duration':duration}})
        
        barges_other.update({f'{count} - {barge}'  : {'Description': f"Refueling Barge", 'Real Time':0, 
                                        'Planification':duration}})
        count =count+1

     
             
    elif "Barge_to_Refueling" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        barge = inst.split("(")[1].split(",")[0]
        origin = inst.split(",")[1].split(")")[0].strip()
        destine = inst.split(",")[2].split(")")[0].strip()
        
        #Planning Result output
        planning_results.update({count : {'Item':count,'Action_InitTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime': dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'barge_to_refueling'
                                        , 'Duration':duration, 'Barge': barge,'Origin': origin, 'Destine': destine, 'Anchorage Occupation':capacity}})
        #Barge Output
        barges_results.update({f'{count} - {barge}'  : {'BargePlannedMovement_name': f"barge_to_refueling", 'BargePlannedMovement_initTime': dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 
                                        'BargePlannedMovement_EndTime': dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"),'BargePlannedMovement_Origin': origin, 'BargePlannedMovement_Destine': destine, 'BargePlannedMovement_Duration':duration}})
        count =count+1
        

    elif "BargeOnSite" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        barge = inst.split("(")[1].split(",")[0]
        origin = inst.split(",")[2].split(")")[0].strip()        
        
        
        #Planning Result output
        planning_results.update({f'{count} - {barge}'  : {'Item':count,'Action_InitTime': dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime': dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'BargeOnSite'
                                        , 'Duration':duration, 'Barge': barge, 'Origin': origin, 'Anchorage Occupation':capacity}})
        
        #Barge Output
        barges_results.update({f'{count} - {barge}'  : {'BargePlannedMovement_name': f"BargeOnSite", 'BargePlannedMovement_initTime': dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 
                                        'BargePlannedMovement_EndTime': dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"),'BargePlannedMovement_Origin': origin,
                                        'BargePlannedMovement_Duration':duration}})
        count =count+1


    elif "Bunkering" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        vessel = inst.split("(")[1].split(",")[0]
        barge = inst.split(",")[1].split(",")[0].strip()
        origin = inst.split(",")[2].split(")")[0].strip()        
        capacity = capacity + 1
        for auxBK, auxBV in listAuxBarge.items():
            if auxBK in barge and (int(initTime) - auxBV)>0 and auxBV > 0:
                planning_results.update({f'w{count} - {barge}' : {'Item':count,'Action_InitTime': dtime.fromtimestamp((int(auxBV)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'Waiting Time Barge'
                                        , 'Duration': int(initTime) - auxBV, 'Barge': barge, 'Origin': origin}})
                barges_results.update({f'w{count} - {barge}' : {'BargePlannedMovement_name': f"Waiting Time Barge", 'BargePlannedMovement_initTime':dtime.fromtimestamp((int(auxBV)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 
                                        'BargePlannedMovement_EndTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"),'BargePlannedMovement_Origin': origin,
                                        'BargePlannedMovement_Duration':int(initTime) - auxBV}})
                for bar in listBarges:
                    if bar.name in barge:
                        bar.waitingTime = auxBV
        listAuxBarge.update({barge:-1})
        count =count+1
        #Planning Result output
        planning_results.update({count : {'Item':count,'Action_InitTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime':dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'Bunkering'
                                        , 'Duration':duration, 'Barge': barge, 'Vessel':vessel, 'Origin': origin, 'Anchorage Occupation':capacity}})
        
        #Barge Output
        barges_results.update({f'{count} - {barge}' : {'BargePlannedMovement_name': f"Bunkering ({vessel})", 'BargePlannedMovement_initTime':dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 
                                        'BargePlannedMovement_EndTime':dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"),'BargePlannedMovement_Origin': origin,
                                        'BargePlannedMovement_Duration':duration}})
        
       

        
        #Vessel Output
        vessels_results.update({f'{vessel}-bunkering': duration}) 
        
        
        count =count+1


    elif "Anch_to_Exit" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        vessel = inst.split("(")[1].split(",")[0]
        origin = inst.split(",")[1].split(")")[0].strip()
        capacity = capacity - 1
        
        #Planning Result output
        planning_results.update({count: {'Item':count,'Action_InitTime': dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime': dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'Anchorage_to_Exit'
                                        , 'Duration':duration, 'Vessel':vessel, 'Origin': origin
                                        , 'Destine':"Exit", 'Anchorage Occupation':capacity}})
        
        #Vessel Output
        vessels_results.update({f'{vessel}-timeLeave':initTime, f'{vessel}-endTime': endTime}) 
        
        count =count+1
 
 
    elif "VesselAtAnchAtStart" in inst:
        initTime = inst.split(":")[0].strip().split(".")[0]
        duration = inst.split("[")[1].split("]")[0].split(".")[0]
        endTime = int(initTime) + int(duration)
        vessel = inst.split("(")[1].split(",")[0]
        origin = inst.split(",")[1].split(")")[0].strip()
         
        capacity = capacity + 1
        
        #Planning Result output
        planning_results.update({count: {'Item':count,'Action_InitTime': dtime.fromtimestamp((int(initTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_EndTime': dtime.fromtimestamp((int(endTime)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Action_Description':'VesselAtAnchAtStart'
                                        , 'Duration':duration, 'Vessel':vessel, 'Origin': origin
                                        , 'Destine':"Exit", 'Anchorage Occupation':capacity}})
        
        count =count+1
 

# Create Output Data

with open(f'{folder}/planning_results.csv', mode='w', newline='') as file:
    writer = csv.DictWriter(file, delimiter=',', fieldnames=['Item', 'Action_InitTime', 'Action_EndTime', 'Action_Description', 'Duration', 'Vessel', 'Barge', 'Origin', 'Destine', 'Anchorage Occupation'] )
    writer.writeheader()


    for res in planning_results.values():
        writer.writerow(res)
        
        
        
for barge in listBarges:    
    with open(f'{folder}/{barge.name}_BargeOtherOperations_results.csv', mode='w', newline='') as file:
        writer = csv.DictWriter(file, delimiter=',', fieldnames=['Description', 'Real Time', 'Planification'])
        writer.writeheader()



        for bargeResKey, bargeResValue in barges_other.items():
            if  barge.name in bargeResKey:
                writer.writerow(bargeResValue)
                
                
                
                

for barge in listBarges:    
    with open(f'{folder}/{barge.name}_BargeActualEvents_results.csv', mode='w', newline='') as file:
        writer = csv.DictWriter(file, delimiter=',', fieldnames=['BargeActualEvent_name', 'BargeActualEvent_initTime', 'BargeActualEvent_EndTime',
                                                                'BargeActualEvent_Origin', 'BargeActualEvent_Destine', 'BargeActualEvent_Duration'])
        writer.writeheader()



        for bargeAct in listBargesActualEvents:
            if  barge.name.lower() in bargeAct.name.lower():
                for key, value in listLocID.items():
                    if key in bargeAct.origin:
                        for key2, value2 in listLocID.items():
                            if key2 in bargeAct.destination:
                                writer.writerow({'BargeActualEvent_name': bargeAct.type, 'BargeActualEvent_initTime': bargeAct.initTime, 'BargeActualEvent_EndTime': bargeAct.endTime,
                                                'BargeActualEvent_Origin': value, 'BargeActualEvent_Destine': value2, 'BargeActualEvent_Duration': bargeAct.duration})
                
    
    
for barge in listBarges:    
    with open(f'{folder}/{barge.name}_BargePlannedEvents_results.csv', mode='w', newline='') as file:
        writer = csv.DictWriter(file, delimiter=',', fieldnames=['BargePlannedMovement_name', 'BargePlannedMovement_initTime', 'BargePlannedMovement_EndTime',
                                                                'BargePlannedMovement_Origin', 'BargePlannedMovement_Destine', 'BargePlannedMovement_Duration'])
        writer.writeheader()



        for bargeResKey, bargeResValue in barges_results.items():
            if  barge.name in bargeResKey:
                writer.writerow(bargeResValue)
    


    


for vessel in listVessels:    
    with open(f'{folder}/{vessel.name}_results.csv', mode='w', newline='') as file:
        writer = csv.DictWriter(file, delimiter=',', fieldnames=['Description','Real Time', 'Planification'])
               
        writer.writeheader()

        rta = None

        itReal = None
        btReal = None
        oa = None
        oaReal = None
        
        if vessel.startAtAnchorage in "False":       
            for vesselResKey, vesselResValue in vessels_results.items():
                if vessel.name == vesselResKey.split('-')[0]:
                    if 'RTA' in vesselResKey:  
                        rta = vesselResValue 
                                          

                        writer.writerow({'Description':'Requested time of arrival (RTA)', 'Planification': dtime.fromtimestamp((int(vesselResValue)*60) + listData[0].start_time).strftime("%Y-%m-%d %H:%M:%S"), 'Real Time':vessel.ataBerth})
                    
                    if 'timeToAnch' in vesselResKey: 
                        writer.writerow({'Description':'Navegation Time to Anchorage', 'Planification': vessel.timeToAnchorage, 'Real Time':vessel.timeToAnchorage})                
                            
                    if 'timeToAnch' in vesselResKey:         
                        writer.writerow({'Description':'Waiting Time', 'Planification':vessel.waitingTime, 'Real Time': vessel.realWaitingTime})
                            
                    if 'timeToAnch' in vesselResKey:             
                        itReal = round((int(datetime.datetime(int(vessel.atsBunk.split('/')[2].split(' ')[0]), int(vessel.atsBunk.split('/')[0]), int(vessel.atsBunk.split('/')[1]), int(vessel.atsBunk.split(' ')[1].split(':')[0]), int(vessel.atsBunk.split(' ')[1].split(':')[1]), 0).timestamp()) - int(datetime.datetime(int(vessel.ataBerth.split('/')[2].split(' ')[0]), int(vessel.ataBerth.split('/')[0]), int(vessel.ataBerth.split('/')[1]), int(vessel.ataBerth.split(' ')[1].split(':')[0]), int(vessel.ataBerth.split(' ')[1].split(':')[1]), 1).timestamp()))/60)
                        writer.writerow({'Description':'Idle Time', 'Planification': 0, 'Real Time': itReal})        
                    
                    if 'bunkering' in vesselResKey:         
                        btReal = (int(datetime.datetime(int(vessel.atcBunk.split('/')[2].split(' ')[0]), int(vessel.atcBunk.split('/')[0]), int(vessel.atcBunk.split('/')[1]), int(vessel.atcBunk.split(' ')[1].split(':')[0]), int(vessel.atcBunk.split(' ')[1].split(':')[1]), 0).timestamp()) - int(datetime.datetime(int(vessel.atsBunk.split('/')[2].split(' ')[0]), int(vessel.atsBunk.split('/')[0]), int(vessel.atsBunk.split('/')[1]), int(vessel.atsBunk.split(' ')[1].split(':')[0]), int(vessel.atsBunk.split(' ')[1].split(':')[1]), 0).timestamp()))/60
                        writer.writerow({'Description':'Bunkering Time', 'Planification':vessel.bunkeringTime, 'Real Time': btReal})          
                        
                    if 'bunkering' in vesselResKey:   
                        writer.writerow({'Description':'Fuel Supplied', 'Planification':vessel.fuelRequested, 'Real Time':vessel.fuelActuallyDelivered})

                    if 'bunkering' in vesselResKey: 
                        oaReal = int(datetime.datetime(int(vessel.atdBerth.split('/')[2].split(' ')[0]), int(vessel.atdBerth.split('/')[0]), int(vessel.atdBerth.split('/')[1]), int(vessel.atdBerth.split(' ')[1].split(':')[0]), int(vessel.atdBerth.split(' ')[1].split(':')[1]), 0).timestamp())/60 - int(datetime.datetime(int(vessel.ataBerth.split('/')[2].split(' ')[0]), int(vessel.atcBunk.split('/')[0]), int(vessel.atcBunk.split('/')[1]), int(vessel.atcBunk.split(' ')[1].split(':')[0]), int(vessel.atcBunk.split(' ')[1].split(':')[1]), 0).timestamp())/60
                        writer.writerow({'Description':'Other Activities', 'Planification':0, 'Real Time': oaReal})
                
                    if 'timeLeave' in vesselResKey: 
                        writer.writerow({'Description':'Navegation Time to leave Anchorage', 'Planification': 15 , 'Real Time': int(datetime.datetime(int(vessel.atdPBP.split('/')[2].split(' ')[0]), int(vessel.atdPBP.split('/')[0]), int(vessel.atdPBP.split('/')[1]), int(vessel.atdPBP.split(' ')[1].split(':')[0]), int(vessel.atdPBP.split(' ')[1].split(':')[1]), 0).timestamp())/60 - int(datetime.datetime(int(vessel.atdBerth.split('/')[2].split(' ')[0]), int(vessel.atdBerth.split('/')[0]), int(vessel.atdBerth.split('/')[1]), int(vessel.atdBerth.split(' ')[1].split(':')[0]), int(vessel.atdBerth.split(' ')[1].split(':')[1]), 0).timestamp())/60})
                    
                    if 'endTime' in vesselResKey: 
                        writer.writerow({'Description':'TOTAL TIME', 'Planification': vessel.waitingTime + vessel.bunkeringTime, 'Real Time': int(vessel.realWaitingTime) + itReal + btReal + oaReal}) 
            

        else:
            for vesselResKey, vesselResValue in vessels_results.items():
                if vessel.name == vesselResKey.split('-')[0]:
                    if 'timeLeave' in vesselResKey: 
                        rta = vesselResValue
                        writer.writerow({'Description':'Requested time of arrival (RTA)', 'Planification': 0, 'Real Time':0})
                        writer.writerow({'Description':'Navigation Time to Delta Anchorage', 'Planification':0, 'Real Time': 0})                
                        writer.writerow({'Description':'Waiting Time', 'Planification':0, 'Real Time': 0})
                        writer.writerow({'Description':'Idle Time', 'Planification':0, 'Real Time':0})        
                            
                        writer.writerow({'Description':'Bunkering Time', 'Planification':0, 'Real Time': 0})  
                        writer.writerow({'Description':'Fuel Supplied', 'Planification':0, 'Real Time':0})
                        writer.writerow({'Description':'Other Activities', 'Planification':vesselResValue, 'Real Time':vesselResValue})
                        writer.writerow({'Description':'Navigation Time to leave Delta Anchorage', 'Planification': 15 , 'Real Time': 15})
                                
                    if 'endTime' in vesselResKey: 
                        writer.writerow({'Description':'TOTAL TIME', 'Planification': vesselResValue, 'Real Time': vesselResValue}) 
                    
                
    
# plot_plan(result.plan, figsize=[20, 15])
